FROM alpine

LABEL org.opencontainers.image.url="https://gitlab.com/LuckyTurtleDev/docker-pushrm-shell/container_registry"
LABEL org.opencontainers.image.title="pushrm on Alpine Linux"
LABEL org.opencontainers.image.source="https://gitlab.com/LuckyTurtleDev/docker-pushrm-shell"

COPY --from=chko/docker-pushrm /docker-pushrm /bin
